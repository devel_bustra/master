import java.awt.Component;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.*;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;


public class StartPanel extends JPanel implements ActionListener {
	
	private static final long serialVersionUID = 1L;

    public JButton start = new JButton("start");	//　スタートボタン
    public JButton rule = new JButton("ルール説明");	//　ルール説明ボタン
    
    
	public StartPanel() {
		// ゲーム枠の作成
		setPreferredSize(new Dimension(240, 320));
		
		add(start);
        start.addActionListener(this);
        
        add(rule);
        rule.addActionListener(this);
        
	}
	
	@Override
	// クリックされたときの処理
	public void actionPerformed(ActionEvent e) {
        	if ( e.getSource() == start ) {
        		
        		Component c = (Component)e.getSource();
        		Window w = SwingUtilities.getWindowAncestor(c);
        		w.dispose();
        		
        		SwingUtilities.invokeLater(() -> {
        			JFrame frame = new JFrame("Bustra!");   
        			frame.add(new Bustra());
        			frame.pack();
        			frame.setVisible(true);

        			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        		});
        		
        	} 
        	
        	if ( e.getSource() == rule ) {
        		JOptionPane.showMessageDialog(
        				this, 
        				"制限時間は10秒"+"\r\n"+"三個消しのみ",
        				"ルール説明",
        		         JOptionPane.INFORMATION_MESSAGE);
        	}
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			JFrame frame = new JFrame("StartPanel");   
			frame.add(new StartPanel());
			frame.pack();
			frame.setVisible(true);

			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		});
    }
}