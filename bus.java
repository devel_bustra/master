//import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.*;
import javax.swing.SwingUtilities;

import java.awt.*;
import static java.awt.Color.*;
import static java.awt.event.KeyEvent.*;
import java.awt.geom.*;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;


public class bus extends JPanel implements Runnable, KeyListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final static int R = 1100, E = 600;
	private final static int COLS = 6, ROWS = 5;	//マスの数
	private boolean toggle = false;
	private Thread thread;
	private MyTimer timer;
	private int x = 0, y = 0;
	//public BufferedImage[][] state;	//盤面位置
    public BufferedImage[] Img = null;	//6種類の画像ファイル（ドロップ）配列
	int [][] state = new int[COLS][ROWS];	//盤面に数字を入れるための二次元配列
	
	public bus() { //コンストラクター
		int i, j;
		
	    try {	//配列にそれぞれ読み込んだ画像を入れる
		      Img = new BufferedImage[6];
		      //state = new BufferedImage[COLS][ROWS];
		      
		      Img[0] = ImageIO.read(new File("img//blue.png"));
		      Img[1] = ImageIO.read(new File("img//red.png"));
		      Img[2] = ImageIO.read(new File("img//green.png"));
		      Img[3] = ImageIO.read(new File("img//violet.png"));
		      Img[4] = ImageIO.read(new File("img//mazenta.png"));
		      Img[5] = ImageIO.read(new File("img//yellow.png"));
		    } catch (Exception e) {
		      e.printStackTrace();
		    }
		
		Bgm.play();	//BGM流す
		startThread();	//スレッド開始
		setPreferredSize(new Dimension(R*COLS*5/100, R*ROWS*5/100+50));	//サイズ設定

		for (i = 0; i < COLS; i++) {	//初期の盤面にランダムで数字を入れる
			for (j = 0; j < ROWS; j++) {
				state[i][j] = (int)(Math.random() * 6);
			}
		}
	
		setFocusable(true);
		addKeyListener(this);

	}
	
	public void startThread() {
		if (thread == null) {
			thread = new Thread(this);
			thread.start();
		}
	}

	@Override
	public void paint(Graphics g) {	//ドロップを描く
	    Graphics2D g2 = (Graphics2D)g;

    	g2.setTransform(AffineTransform.getScaleInstance(0.05, 0.05));	//画像のサイズ変更(0.05倍)
	    
	    int i, j;	//縦i 横j
	   
	    for (i = 0; i < COLS; i++) {
			for (j = 0; j < ROWS; j++) {
				if ( x == i && y == j ) {
					if (toggle) {
						g.setColor(BLACK);	//時間内なら黒枠
					} else {
						g.setColor(LIGHT_GRAY);	//時間外ならグレー
					}
				} else {
					g.setColor(WHITE);	//その他のドロップは白枠
				}
				g.fillOval(i * R, j * R+E, R, R);	//ドロップの枠線を描く
				g2.drawImage(Img[state[i][j]], i * R, j * R + E, null);	//位置の数字を受け取りドロップを描く
			}
		}

		g.setColor(BLACK);
		g.drawString("← ↑ ↓ →  : move position", 20, ROWS * R + 25);	//文字表示
		g.drawString("<SPACE>   : toggle exchange",  20, ROWS * R + 40);
	}
	
	@Override
	public void run() {
		Thread thisThread = Thread.currentThread();
		while (thisThread == thread) {
			if ( (timer != null) && (timer.getFlag() == true) ) { 
			    timer.setFlag(false);
				toggle = !toggle;
				// 
				}
			repaint();
			
			try{
				Thread.sleep(50);
			} catch(InterruptedException e) {}
		}
	}

	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		switch (key) {
		case VK_SPACE: 
			if (!toggle) {
				timer = new MyTimer();
			}
			toggle = !toggle;
			break;
		case VK_LEFT:  
			if (x > 0) {
				x--;
				if (toggle) {
					int tmp = state[x + 1][y];
					state[x + 1][y] = state[x][y];
					state[x][y] = tmp;
					Sound.play();
				}
			}
			break;
		case VK_UP:   
			if (y > 0) {
				y--; 
				if (toggle) {
					int tmp = state[x][y + 1];
					state[x][y + 1] = state[x][y];
					state[x][y] = tmp;
					Sound.play();
				}
			}
			break;
		case VK_DOWN:  
			if (y < ROWS - 1) {
				y++; 
				if (toggle) {
					int tmp = state[x][y - 1];
					state[x][y - 1] = state[x][y];
					state[x][y] = tmp;
					Sound.play();
				}
			}
			break;
		case VK_RIGHT: 
			if (x < COLS - 1) {
				x++;

				if (toggle) {
					int tmp = state[x - 1][y];
					state[x - 1][y] = state[x][y];
					state[x][y] = tmp;
					Sound.play();
				}
			}
			break;
		default: 
			break;
		}
	}

	public void keyReleased(KeyEvent e) {}

	public void keyTyped(KeyEvent e) {}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			JFrame frame = new JFrame("Bustra!");       

			frame.add(new bus());
			frame.pack();
			frame.setVisible(true);

			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
		});
	}
}