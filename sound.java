//http://maedakobe.rw.xsi.jp/java2/soundclip.htm
import java.io.File;
import java.io.IOException;
import javax.sound.sampled.*;

public class Sound
{
	private static File file = new File("sound.wav");
	private static AudioInputStream stream;
	private static Clip clip = null;
    private static DataLine.Info info;
    private static AudioFormat audioFormat;
    
    public static void play(String[] args)
    {
		try
        {   
			stream = AudioSystem.getAudioInputStream(file);
            audioFormat = stream.getFormat();
            info = new DataLine.Info(Clip.class, audioFormat);
            clip = (Clip)AudioSystem.getLine(info);
            clip.open(stream);
            clip.start();
        }
        catch (UnsupportedAudioFileException x)
        {   x.printStackTrace();  }
        catch (IOException x)
        {   x.printStackTrace();  }
        catch (LineUnavailableException x)
        {   x.printStackTrace();  }
    }
}